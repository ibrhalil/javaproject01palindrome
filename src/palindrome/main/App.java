package palindrome.main;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import palindrome.gui.Gui;

public class App {

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException 
	{
		
		JFrame ekran = new JFrame("Palindrom Bulma");
		
		ekran.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //ekran.setResizable(false);
		ekran.add(new Gui());
		ekran.setSize(700, 400); 
		
		//UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		//UIManager.setLookAndFeel(UIManager.getLookAndFeel());
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());	//bulundu�u i�letim sistemine g�re �al���r
		
        System.out.println();
		
		ekran.setVisible(true);
		
		
		/*
		palindrome.api.Palindrome.isPalindrome("Aba");
		palindrome.api.Palindrome.isPalindrome("Ada");
		palindrome.api.Palindrome.isPalindrome("A�a");
		palindrome.api.Palindrome.isPalindrome("Ala");
		palindrome.api.Palindrome.isPalindrome("�m�");
		palindrome.api.Palindrome.isPalindrome("Ana");
		palindrome.api.Palindrome.isPalindrome("Ara");
		palindrome.api.Palindrome.isPalindrome("�s�");
		palindrome.api.Palindrome.isPalindrome("Ata");
		palindrome.api.Palindrome.isPalindrome("Aya");
		palindrome.api.Palindrome.isPalindrome("�z�");
		palindrome.api.Palindrome.isPalindrome("Ebe");
		palindrome.api.Palindrome.isPalindrome("Ece");
		palindrome.api.Palindrome.isPalindrome("Efe");
		palindrome.api.Palindrome.isPalindrome("Elle");
		palindrome.api.Palindrome.isPalindrome("�ki");
		palindrome.api.Palindrome.isPalindrome("�ri");
		palindrome.api.Palindrome.isPalindrome("�yi");
		palindrome.api.Palindrome.isPalindrome("Kabak");
		palindrome.api.Palindrome.isPalindrome("Ka�ak");
		palindrome.api.Palindrome.isPalindrome("Kak");
		palindrome.api.Palindrome.isPalindrome("Kavak");
		palindrome.api.Palindrome.isPalindrome("Kayak");
		palindrome.api.Palindrome.isPalindrome("Kazak");
		palindrome.api.Palindrome.isPalindrome("Kek");
		palindrome.api.Palindrome.isPalindrome("Kelek");
		palindrome.api.Palindrome.isPalindrome("K�ll�k");
		palindrome.api.Palindrome.isPalindrome("K�l�k");
		palindrome.api.Palindrome.isPalindrome("K�r�k");
		palindrome.api.Palindrome.isPalindrome("K�s�k");
		palindrome.api.Palindrome.isPalindrome("Kulluk");
		palindrome.api.Palindrome.isPalindrome("K���k");
		palindrome.api.Palindrome.isPalindrome("K�t�k");
		palindrome.api.Palindrome.isPalindrome("K�ll�k");
		palindrome.api.Palindrome.isPalindrome("L�l");
		palindrome.api.Palindrome.isPalindrome("Makam");
		palindrome.api.Palindrome.isPalindrome("Mamam");
		palindrome.api.Palindrome.isPalindrome("Mum");
		palindrome.api.Palindrome.isPalindrome("Nacican");
		palindrome.api.Palindrome.isPalindrome("Nalan");
		palindrome.api.Palindrome.isPalindrome("Nazan");
		palindrome.api.Palindrome.isPalindrome("Neden");
		palindrome.api.Palindrome.isPalindrome("Ni�in");
		palindrome.api.Palindrome.isPalindrome("Radar");
		palindrome.api.Palindrome.isPalindrome("Ses");
		palindrome.api.Palindrome.isPalindrome("Sis");
		palindrome.api.Palindrome.isPalindrome("Sos");
		palindrome.api.Palindrome.isPalindrome("Sus");
		palindrome.api.Palindrome.isPalindrome("S�s");
		palindrome.api.Palindrome.isPalindrome("Talat");
		palindrome.api.Palindrome.isPalindrome("Tat");
		palindrome.api.Palindrome.isPalindrome("�t�");
		
		palindrome.api.Palindrome.isPalindrome("Anastas mum satsana.");
		palindrome.api.Palindrome.isPalindrome("�la� i� Ali.");
		palindrome.api.Palindrome.isPalindrome("Ey Edip Adana'da pide ye.");
		palindrome.api.Palindrome.isPalindrome("Ey Nihat Adana'da tahin ye.");
		palindrome.api.Palindrome.isPalindrome("Ara piller eder elli para.");
		palindrome.api.Palindrome.isPalindrome("Para haz�r ama R�za harap.");
		palindrome.api.Palindrome.isPalindrome("En iyi me�e be�e mi yine?");
		palindrome.api.Palindrome.isPalindrome("Ayla'da m� madalya?");
		palindrome.api.Palindrome.isPalindrome("Al kaz�k �ak karaya kayarak ka� k�zakla.");
		palindrome.api.Palindrome.isPalindrome("Ma�ara daha dar a�am.");
		palindrome.api.Palindrome.isPalindrome("A� raporunu koy, okunur o par�a.");
		palindrome.api.Palindrome.isPalindrome("R�za, Haluk okula haz�r.");
		
		*/

		/*
		 
		Anastas mum satsana.
		�la� i� Ali.
		Ey Edip Adana'da pide ye.
		Ey Nihat Adana'da tahin ye.
		Ara piller eder elli para.
		Para haz�r ama R�za harap.
		En iyi me�e be�e mi yine?
		Ayla'da m� madalya?
		Al kaz�k �ak karaya kayarak ka� k�zakla.
		Ma�ara daha dar a�am.
		A� raporunu koy, okunur o par�a.
		R�za, Haluk okula haz�r.
		 
		 */
	}

}
