package palindrome.api;

public class Palindrome 
{

	static public boolean isPalindrome(String text)
	{
		text = text.trim().toLowerCase();
		String txt = String.valueOf(text);
		
		String reverse = "";
		String reverseNot = "";
		
		for (int i = 0; i < txt.length(); i++) 
		{
			int d = txt.codePointAt(i);
			if(d !=32 && d !=34 && d !=39 && d !=44 && d != 46 && d !=63)
			{
				reverseNot+=txt.charAt(i);
				reverse = txt.charAt(i) + reverse;
			}
		}
		

		
		
		
		//System.out.println(text);
		/*System.out.println(txt);
		
		System.out.println(reverseNot);
		System.out.println(reverse);*/
		
		//System.out.println(reverseNot.equals(reverse.toString()));
		//System.out.println("\n-------\n");
		
		return reverseNot.equals(reverse.toString());
	}
	
}
