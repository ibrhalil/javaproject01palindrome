package palindrome.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import palindrome.api.Palindrome;

public class Gui extends JPanel implements ActionListener, KeyListener
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField input; 
	private JLabel output; 
	private JLabel label1;
	private JButton button1;
	
	public Gui() 
	{
		label1 = new JLabel("C�mle :"); 
		label1.setFont(new Font("Arial", Font.PLAIN, 20)); 
        label1.setSize(100, 20); 
        label1.setLocation(100, 100); 
        this.add(label1); 
		
        input = new JTextField(); 
        input.setFont(new Font("Arial", Font.PLAIN, 15)); 
        input.setSize(250, 20); 
        input.setLocation(200, 100); 
        input.addKeyListener(this);
	    this.add(input); 
	    
	    button1 = new JButton("Kontrol"); 
	    button1.setFont(new Font("Arial", Font.PLAIN, 15)); 
	    button1.setSize(100, 20); 
	    button1.setLocation(200, 150); 
	    button1.addActionListener(this); 
	    
        this.add(button1); 
        
        output = new JLabel(); 
        output.setFont(new Font("Arial", Font.PLAIN, 15)); 
        output.setSize(150, 20); 
        output.setLocation(200, 200); 
	    this.add(output);
	    setLayout(null); 
	}	
	
	private void islem() 
	{
		if(Palindrome.isPalindrome(input.getText()))
		{
			output.setText("Polindrom c�mlesidir.");
			output.setForeground(Color.magenta);
		}
		else {
			output.setText("De�ildir!");
			output.setForeground(Color.RED);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == button1 && input.getText().length()>0)
		{
			islem();
		}
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) 
	{
		if (e.getKeyCode()==KeyEvent.VK_ENTER)
		{
            islem();
        }
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
